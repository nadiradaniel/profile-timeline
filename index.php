<?php 
  $data = [
    [
      'name' => 'Nadira',
      'photo' => 'default.jpg'
    ],
    [
      'name' => 'Nadira',
      'photo' => 'default.jpg'
    ],
    [
      'name' => 'Nadira',
      'photo' => 'default.jpg'
    ]
  ];
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Profile</title> 
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <section class="intro">
  <div class="container">
    <h1>Profile Timeline &darr;</h1>
  </div>
</section>

<section class="timeline">
  <ul>
  <?php 
    foreach($data as $sub) {
        $names = array_column($data, 'name');
        $photos = array_column($data, 'photo');    
    }

    for ($i=0; $i < count($names) ; $i++) {
       echo  "<li> <div> <img class='img-circle' src='images/".$photos[$i]."'>".$names[$i]."</div> </li>";
    }

  ?>
  </ul>
</section> 
    <script src="js/index.js"></script>
</body>
</html>
